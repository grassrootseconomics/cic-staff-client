// Core imports
import { TestBed } from '@angular/core/testing';

// Application imports
import { RoleGuard } from '@app/_guards/role.guard';

describe('RoleGuard', () => {
  let guard: RoleGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RoleGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
