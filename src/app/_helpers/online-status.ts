const apiUrls = [
  'https://api.coindesk.com/v1/bpi/currentprice.json',
  'https://dog.ceo/api/breeds/image/random',
];

async function checkOnlineStatus(): Promise<boolean> {
  return navigator.onLine;
  // TODO: Find better way of checking for device internet connectivity.
  // try {
  //   const online = await fetch(apiUrls[Math.floor(Math.random() * apiUrls.length)]);
  //   return online.status >= 200 && online.status < 300;
  // } catch (error) {
  //   return false;
  // }
}

export { checkOnlineStatus };
