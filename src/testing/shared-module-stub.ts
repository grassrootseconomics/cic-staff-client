import { Component } from '@angular/core';

@Component({ selector: 'app-sidebar', template: '' })
export class SidebarStubComponent {}

@Component({ selector: 'app-topbar', template: '' })
export class TopbarStubComponent {}

@Component({ selector: 'app-footer', template: '' })
export class FooterStubComponent {}
